# RandomOrg

Access true random numbers through the random.org API!

See https://random.org/ for the specifics of the random.org services and their [API](https://api.random.org/json-rpc/1).

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     randomorg:
       github: your-github-user/randomorg
   ```

2. Run `shards install`

## Usage

```crystal
require "randomorg"
```

Before you can get your random numbers, you will need an API key. You can request one from here: https://api.random.org/api-keys/beta

Then you need to configure the module with this key in order to use the service:

```crystal
RandomOrg.configure do |config|
  config.api_key = "YOUR_API_KEY"
end
```

After which you can use it in pretty much the same way as you would the SecureRandom library:

```crystal
> RandomOrg.random_number(100)
=> 78
> RandomOrg.base64
=> "r1nwqJksqKacn26UBI1GkQ=="
```

## Development

Nothing special to think about.

## Contributing

1. Fork it (<https://bitbucket.org/janlindblom/crystal-randomorg>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Jan Lindblom](https://bitbucket.org/janlindblom) - creator and maintainer

## License

The shard is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

Use of the random.org service and API is subject to their Terms and Conditions: https://www.random.org/terms/
