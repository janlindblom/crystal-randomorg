require "./random_org/configuration"

# This library is an interface to the random.org random number generator API
# which generates true random numbers through data gathered from atmospheric
# noise.
#
# This library is implemented as a drop-in replacement for SecureRandom, giving
# you the same methods with the same parameters and mimicing the behaviour of
# the corresponding method in SecureRandom.
module RandomOrg
  VERSION = "0.2.1"

  # Modify the current configuration.
  #
  # @example
  #   RandomOrg.configure do |config|
  #     config.api_key = "YOUR_API_KEY"
  #   end
  #
  # @yieldparam [RandomOrg::Configuration] config current configuration
  def self.configure
    Config.configuration ||= RandomOrg::Configuration.new
    yield Config.configuration
  end

  # Raised when there is a problem with input arguments.
  class ArgumentError < StandardError
  end

  # Raised when there is a problem accessing the API.
  class ApiError < StandardError
  end

  # Raised when the API service endpoint is experiencing problems.
  class ApiServerError < StandardError
  end

  # Raised when the API returns an error due to a nonexisting API key.
  class WrongApiKeyError < StandardError
  end

  private class Config
    property configuration
  end

  private def self.bad_response_error(response)
    raise ApiError, "Something is wrong with the response: #{response}"
  end
end
