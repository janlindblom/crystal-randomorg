module RandomOrg
  # Holds configuration of the module required for it to work.
  class Configuration
    # Sets the random.org API key.
    # @return [String]
    property api_key
  
    def initialize
      @api_key = ""
    end
  end
end
  